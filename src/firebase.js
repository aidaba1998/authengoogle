
import { initializeApp } from "firebase/app";
import{ getAuth, GoogleAuthProvider ,signInWithPopup } from "firebase/auth";
const firebaseConfig = {
  apiKey: "AIzaSyB2cwOMGwT5U5T8IuPJBcV873Kkjougo40",
  authDomain: "auth-eb242.firebaseapp.com",
  projectId: "auth-eb242",
  storageBucket: "auth-eb242.appspot.com",
  messagingSenderId: "172606405848",
  appId: "1:172606405848:web:5bce50b6253a5c32542bad"
};

const app = initializeApp(firebaseConfig);
export const auth=getAuth(app);
const provider=new GoogleAuthProvider()
 export const signInWithGoogle=()=>{
    signInWithPopup(auth,provider)
    .then((result)=>{
    const name=result.user.displayName;
    const email=result.user.email;
    const profilePic=result.user.photoURL;
    localStorage.setItem (`name`,name)
    localStorage.setItem (`email`,email)
    localStorage.setItem (`profilePic`,profilePic)
    }).catch((error)=>{
        console.log(error);
    })
}