import React from 'react';
import {signInWithGoogle} from './firebase'
import 'bootstrap/dist/css/bootstrap.min.css';


function App() {
  return (
    <div className='text-center'>
      <button className='mt-4 rounded-pill p-3  btn-info' onClick={signInWithGoogle}>Sign In With Google</button>
      <h1>{localStorage.getItem(`name`)}</h1>
      <h1>{localStorage.getItem(`email`)}</h1>
      <img src={localStorage.getItem(`profilePic`)}/>
    </div>
  )
}

export default App
